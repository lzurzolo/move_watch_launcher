﻿using IniParser;
using IniParser.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace move_watch_launcher
{
    public enum HandOptions
    {
        E_NONE,
        E_ENABLE_BOTH,
        E_DISABLE_LEFT,
        E_DISABLE_RIGHT
    };

    public enum Version
    {
        E_NONE,
        E_LEAP_VR,
        E_LEAP_NON_VR,
        E_5DT_GLOVE,
        E_CYBER_GLOVE
    };

    public partial class Form1 : Form
    {
        private bool UseDAQ = false;
        private bool UseInvisibleHands = false;
        private HandOptions SelectedHandOption = HandOptions.E_NONE;

        private string validConfigSchema;
        private string currentlySelecteConfigPath;
        private string subjectName;
        private string trialNumber;
        private string logFileName;
        private Version SelectedVersion = Version.E_NONE;

        private static readonly char[] InvalidChars = "<>:\"/\\|?*".ToCharArray();

        public Form1()
        {
            InitializeComponent();
            LoadValidSchema();
            grpDAQoptions.Visible = false;
        }

        private bool HasObservePrompts(JArray ja, HandOptions ho)
        {
            if(ho == HandOptions.E_ENABLE_BOTH)
            {
                return false;
            }

            if(ho == HandOptions.E_DISABLE_LEFT)
            {
                foreach(var item in ja.Children())
                {
                    if(item.Value<string>("WhichHand") == "Left" && item.Value<bool>("Observe") == true)
                    {
                        return true;
                    }
                }
                return false;
            }

            if(ho == HandOptions.E_DISABLE_RIGHT)
            {
                foreach (var item in ja.Children())
                {
                    if (item.Value<string>("WhichHand") == "Right" && item.Value<bool>("Observe") == true)
                    {
                        return true;
                    }
                }
                return false;
            }

            return false;
        }

        private bool HasBothPrompts(JArray ja, HandOptions ho)
        {
            if(ho == HandOptions.E_ENABLE_BOTH)
            {
                return false;
            }
            
            if(ho == HandOptions.E_DISABLE_LEFT || ho == HandOptions.E_DISABLE_RIGHT)
            {
                foreach (var item in ja.Children())
                {
                    if (item.Value<string>("WhichHand") == "Both")
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void btnLaunch_Click(object sender, EventArgs e)
        {
            bool valid = false;

            if(string.IsNullOrWhiteSpace(txtSubjectName.Text))
            {
                MessageBox.Show("Enter a subject name");
                return;
            }

            if(string.IsNullOrWhiteSpace(txtTrialNumber.Text))
            {
                MessageBox.Show("Enter a trial number");
                return;
            }

            subjectName = txtSubjectName.Text;
            if(ContainsInvalidCharacters(subjectName))
            {
                MessageBox.Show("Subject name contains invalid characters");
                return;
            }

            if(Int32.TryParse(txtTrialNumber.Text, out int n))
            {
                trialNumber = txtTrialNumber.Text;
            }
            else
            {
                MessageBox.Show("Invalid trial number");
                return;
            }

            if(SelectedHandOption == HandOptions.E_NONE)
            {
                MessageBox.Show("You must select a Hand Option");
                return;
            }

            logFileName = subjectName + "_" + trialNumber;

            JSchema schema = JSchema.Parse(validConfigSchema);
            JArray jsonConfig = JArray.Parse(File.ReadAllText(currentlySelecteConfigPath));
            IList<string> errors;
            valid = jsonConfig.IsValid(schema, out errors);
            if (valid)
            {
                // check if the config file contains illegal observe movements with a disabled hand
                if(HasObservePrompts(jsonConfig, SelectedHandOption))
                {
                    MessageBox.Show("Cannot select " + SelectedHandOption + " while there are Observe prompts for this hand");
                    return;
                }

                // check if the config file contains illegal both momements with a disabled hand
                if(HasBothPrompts(jsonConfig, SelectedHandOption))
                {
                    MessageBox.Show("Cannot select " + SelectedHandOption + " while there are Both prompts");
                    return;
                }

                if(SelectedVersion == Version.E_NONE)
                {
                    MessageBox.Show("You must select a version");
                    return;
                }
                else if (SelectedVersion == Version.E_LEAP_VR)
                {
                    File.Copy(currentlySelecteConfigPath, @"binvr\\move_watch_Data\\config.json", true);
                    Process moveWatch = new Process();
                    moveWatch.StartInfo.FileName = @"binvr\\move_watch.exe";
                    if (UseDAQ && UseInvisibleHands)
                        moveWatch.StartInfo.Arguments = logFileName + " daq invisible";
                    else if (UseDAQ)
                        moveWatch.StartInfo.Arguments = logFileName + " daq";
                    else if (UseInvisibleHands)
                        moveWatch.StartInfo.Arguments = logFileName + " invisible";
                    else
                        moveWatch.StartInfo.Arguments = logFileName;
                    RestartLeapService();
                    moveWatch.Start();
                }
                else if (SelectedVersion == Version.E_LEAP_NON_VR)
                {
                    File.Copy(currentlySelecteConfigPath, @"bin\\move_watch_Data\\config.json", true);
                    Process moveWatch = new Process();
                    moveWatch.StartInfo.FileName = @"bin\\move_watch.exe";
                    string args = "";
                    if (UseDAQ)
                        args += "-daq ";
                    args += "--logFileName ";
                    args += logFileName;
                    args += " ";
                    if (UseInvisibleHands)
                        args += "-invisible ";
                    args += "--hands ";
                    if (SelectedHandOption == HandOptions.E_ENABLE_BOTH)
                    {
                        args += "enabled";
                    }
                    else if (SelectedHandOption == HandOptions.E_DISABLE_LEFT)
                    {
                        args += "disableLeft";
                    }
                    else if (SelectedHandOption == HandOptions.E_DISABLE_RIGHT)
                    {
                        args += "disableRight";
                    }
                    
                    RestartLeapService();
                    moveWatch.StartInfo.Arguments = args;
                    moveWatch.Start();
                }
                else if(SelectedVersion == Version.E_5DT_GLOVE || SelectedVersion == Version.E_CYBER_GLOVE)
                {
                    File.Copy(currentlySelecteConfigPath, @"binglove\\move_watch_Data\\config.json", true);
                    Process moveWatch = new Process();
                    moveWatch.StartInfo.FileName = @"binglove\\move_watch.exe";
                    string args = "";
                    if (UseDAQ)
                        args += "-daq ";
                    args += "--logFileName ";
                    args += logFileName;
                    args += " ";
                    if (UseInvisibleHands)
                        args += "-invisible ";
                    args += "--hands ";
                    if (SelectedHandOption == HandOptions.E_ENABLE_BOTH)
                    {
                        args += "enabled";
                    }
                    else if (SelectedHandOption == HandOptions.E_DISABLE_LEFT)
                    {
                        args += "disableLeft";
                    }
                    else if (SelectedHandOption == HandOptions.E_DISABLE_RIGHT)
                    {
                        args += "disableRight";
                    }
                    args += " --glove ";
                    if(SelectedVersion == Version.E_5DT_GLOVE)
                    {
                        args += "5dt";
                    }
                    else
                    {
                        args += "cyberglove";
                    }
                    moveWatch.StartInfo.Arguments = args;
                    moveWatch.Start();
                }
                WriteDAQSettings();
            }
            else
            {
                MessageBox.Show("Not a valid config file");
            }
        }

        private void btnSelectConfig_Click(object sender, EventArgs e)
        {
            // Present the user with the open file dialog and allow selection of a config file
            string openPath = System.AppDomain.CurrentDomain.BaseDirectory + "config";
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = openPath,
                Filter = "JSON Configuration | *.json"
            };

            // Write the file name to the text box
            if(openFileDialog.ShowDialog() == DialogResult.OK)
            {
                currentlySelecteConfigPath = openFileDialog.FileName;
                Console.WriteLine(currentlySelecteConfigPath);
                txtConfiguration.Text = Path.GetFileName(openFileDialog.FileName);
            }
        }

        private void rdoVR_CheckedChanged(object sender, EventArgs e)
        {
            SelectedVersion = Version.E_LEAP_VR;
        }

        private void rdoNonVR_CheckedChanged(object sender, EventArgs e)
        {
            SelectedVersion = Version.E_LEAP_NON_VR;
        }

        private void rdoCyberGlove_CheckedChanged(object sender, EventArgs e)
        {
            SelectedVersion = Version.E_CYBER_GLOVE;
        }

        private void rdo5DT_CheckedChanged(object sender, EventArgs e)
        {
            SelectedVersion = Version.E_5DT_GLOVE;
        }

        private bool ValidateConfigFile()
        {
            return false;
        }

        private void LoadValidSchema()
        {
            Console.WriteLine("Loading schema");
            string schemaFile = System.AppDomain.CurrentDomain.BaseDirectory + "schema.json";
            validConfigSchema = File.ReadAllText(schemaFile);
        }

        private void chkUseDAQ_CheckedChanged(object sender, EventArgs e)
        {
            if(chkUseDAQ.Checked)
            {
                UseDAQ = true;
                grpDAQoptions.Visible = true;
            }

            if(!chkUseDAQ.Checked)
            {
                UseDAQ = false;
                grpDAQoptions.Visible = false;
            }
        }

        public static bool ContainsInvalidCharacters(string text)
        {
            return text.IndexOfAny(InvalidChars) >= 0;
        }

        private void chkInvisibleMirror_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInvisibleMirror.Checked)
                UseInvisibleHands = true;

            if (!chkInvisibleMirror.Checked)
                UseInvisibleHands = false;
        }

        private void RestartLeapService()
        {
            var proc = new ProcessStartInfo();
            proc.UseShellExecute = true;
            proc.WorkingDirectory = @"C:\Program Files\Leap Motion\Core Services";
            proc.FileName = @"C:\Program Files\Leap Motion\Core Services\LeapSvc.exe";
            proc.Verb = "runas";
            proc.Arguments = "--run";
            proc.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(proc);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            SelectedHandOption = HandOptions.E_ENABLE_BOTH;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            SelectedHandOption = HandOptions.E_DISABLE_LEFT;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            SelectedHandOption = HandOptions.E_DISABLE_RIGHT;
        }

        private void WriteDAQSettings()
        {
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("daq-settings.ini");
            data["Comm"]["DeviceNumber"] = nudDeviceNumber.Value.ToString();
            data["Comm"]["Port"] = nudPortNumber.Value.ToString();
            parser.WriteFile("daq-settings.ini", data);
        }
    }
}
