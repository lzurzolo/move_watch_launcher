﻿namespace move_watch_launcher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLaunch = new System.Windows.Forms.Button();
            this.txtConfiguration = new System.Windows.Forms.TextBox();
            this.btnSelectConfig = new System.Windows.Forms.Button();
            this.grpVR = new System.Windows.Forms.GroupBox();
            this.rdoCyberGlove = new System.Windows.Forms.RadioButton();
            this.rdo5DT = new System.Windows.Forms.RadioButton();
            this.rdoNonVR = new System.Windows.Forms.RadioButton();
            this.rdoVR = new System.Windows.Forms.RadioButton();
            this.chkUseDAQ = new System.Windows.Forms.CheckBox();
            this.grpLogging = new System.Windows.Forms.GroupBox();
            this.txtTrialNumber = new System.Windows.Forms.TextBox();
            this.lblTrialNumber = new System.Windows.Forms.Label();
            this.txtSubjectName = new System.Windows.Forms.TextBox();
            this.lblSubjectName = new System.Windows.Forms.Label();
            this.chkInvisibleMirror = new System.Windows.Forms.CheckBox();
            this.grpHandOptions = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.grpDAQoptions = new System.Windows.Forms.GroupBox();
            this.lblDeviceNumber = new System.Windows.Forms.Label();
            this.nudDeviceNumber = new System.Windows.Forms.NumericUpDown();
            this.lblPortNumber = new System.Windows.Forms.Label();
            this.nudPortNumber = new System.Windows.Forms.NumericUpDown();
            this.grpVR.SuspendLayout();
            this.grpLogging.SuspendLayout();
            this.grpHandOptions.SuspendLayout();
            this.grpDAQoptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDeviceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPortNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLaunch
            // 
            this.btnLaunch.Location = new System.Drawing.Point(12, 308);
            this.btnLaunch.Name = "btnLaunch";
            this.btnLaunch.Size = new System.Drawing.Size(75, 23);
            this.btnLaunch.TabIndex = 0;
            this.btnLaunch.Text = "Launch";
            this.btnLaunch.UseVisualStyleBackColor = true;
            this.btnLaunch.Click += new System.EventHandler(this.btnLaunch_Click);
            // 
            // txtConfiguration
            // 
            this.txtConfiguration.Location = new System.Drawing.Point(12, 12);
            this.txtConfiguration.Name = "txtConfiguration";
            this.txtConfiguration.ReadOnly = true;
            this.txtConfiguration.Size = new System.Drawing.Size(212, 20);
            this.txtConfiguration.TabIndex = 1;
            // 
            // btnSelectConfig
            // 
            this.btnSelectConfig.Location = new System.Drawing.Point(230, 12);
            this.btnSelectConfig.Name = "btnSelectConfig";
            this.btnSelectConfig.Size = new System.Drawing.Size(84, 23);
            this.btnSelectConfig.TabIndex = 2;
            this.btnSelectConfig.Text = "Select Config";
            this.btnSelectConfig.UseVisualStyleBackColor = true;
            this.btnSelectConfig.Click += new System.EventHandler(this.btnSelectConfig_Click);
            // 
            // grpVR
            // 
            this.grpVR.Controls.Add(this.rdoCyberGlove);
            this.grpVR.Controls.Add(this.rdo5DT);
            this.grpVR.Controls.Add(this.rdoNonVR);
            this.grpVR.Controls.Add(this.rdoVR);
            this.grpVR.Location = new System.Drawing.Point(12, 39);
            this.grpVR.Name = "grpVR";
            this.grpVR.Size = new System.Drawing.Size(112, 117);
            this.grpVR.TabIndex = 3;
            this.grpVR.TabStop = false;
            this.grpVR.Text = "Version";
            // 
            // rdoCyberGlove
            // 
            this.rdoCyberGlove.AutoSize = true;
            this.rdoCyberGlove.Location = new System.Drawing.Point(7, 88);
            this.rdoCyberGlove.Name = "rdoCyberGlove";
            this.rdoCyberGlove.Size = new System.Drawing.Size(80, 17);
            this.rdoCyberGlove.TabIndex = 3;
            this.rdoCyberGlove.TabStop = true;
            this.rdoCyberGlove.Text = "CyberGlove";
            this.rdoCyberGlove.UseVisualStyleBackColor = true;
            this.rdoCyberGlove.CheckedChanged += new System.EventHandler(this.rdoCyberGlove_CheckedChanged);
            // 
            // rdo5DT
            // 
            this.rdo5DT.AutoSize = true;
            this.rdo5DT.Location = new System.Drawing.Point(7, 65);
            this.rdo5DT.Name = "rdo5DT";
            this.rdo5DT.Size = new System.Drawing.Size(103, 17);
            this.rdo5DT.TabIndex = 2;
            this.rdo5DT.TabStop = true;
            this.rdo5DT.Text = "5DT Data Glove";
            this.rdo5DT.UseVisualStyleBackColor = true;
            this.rdo5DT.CheckedChanged += new System.EventHandler(this.rdo5DT_CheckedChanged);
            // 
            // rdoNonVR
            // 
            this.rdoNonVR.AutoSize = true;
            this.rdoNonVR.Location = new System.Drawing.Point(7, 42);
            this.rdoNonVR.Name = "rdoNonVR";
            this.rdoNonVR.Size = new System.Drawing.Size(90, 17);
            this.rdoNonVR.TabIndex = 1;
            this.rdoNonVR.TabStop = true;
            this.rdoNonVR.Text = "Leap Non-VR";
            this.rdoNonVR.UseVisualStyleBackColor = true;
            this.rdoNonVR.CheckedChanged += new System.EventHandler(this.rdoNonVR_CheckedChanged);
            // 
            // rdoVR
            // 
            this.rdoVR.AutoSize = true;
            this.rdoVR.Location = new System.Drawing.Point(7, 19);
            this.rdoVR.Name = "rdoVR";
            this.rdoVR.Size = new System.Drawing.Size(67, 17);
            this.rdoVR.TabIndex = 0;
            this.rdoVR.TabStop = true;
            this.rdoVR.Text = "Leap VR";
            this.rdoVR.UseVisualStyleBackColor = true;
            this.rdoVR.CheckedChanged += new System.EventHandler(this.rdoVR_CheckedChanged);
            // 
            // chkUseDAQ
            // 
            this.chkUseDAQ.AutoSize = true;
            this.chkUseDAQ.Location = new System.Drawing.Point(130, 39);
            this.chkUseDAQ.Name = "chkUseDAQ";
            this.chkUseDAQ.Size = new System.Drawing.Size(85, 17);
            this.chkUseDAQ.TabIndex = 4;
            this.chkUseDAQ.Text = "Use NI-DAQ";
            this.chkUseDAQ.UseVisualStyleBackColor = true;
            this.chkUseDAQ.CheckedChanged += new System.EventHandler(this.chkUseDAQ_CheckedChanged);
            // 
            // grpLogging
            // 
            this.grpLogging.Controls.Add(this.txtTrialNumber);
            this.grpLogging.Controls.Add(this.lblTrialNumber);
            this.grpLogging.Controls.Add(this.txtSubjectName);
            this.grpLogging.Controls.Add(this.lblSubjectName);
            this.grpLogging.Location = new System.Drawing.Point(12, 163);
            this.grpLogging.Name = "grpLogging";
            this.grpLogging.Size = new System.Drawing.Size(193, 116);
            this.grpLogging.TabIndex = 5;
            this.grpLogging.TabStop = false;
            this.grpLogging.Text = "Logging";
            // 
            // txtTrialNumber
            // 
            this.txtTrialNumber.Location = new System.Drawing.Point(6, 90);
            this.txtTrialNumber.Name = "txtTrialNumber";
            this.txtTrialNumber.Size = new System.Drawing.Size(67, 20);
            this.txtTrialNumber.TabIndex = 3;
            // 
            // lblTrialNumber
            // 
            this.lblTrialNumber.AutoSize = true;
            this.lblTrialNumber.Location = new System.Drawing.Point(6, 74);
            this.lblTrialNumber.Name = "lblTrialNumber";
            this.lblTrialNumber.Size = new System.Drawing.Size(67, 13);
            this.lblTrialNumber.TabIndex = 2;
            this.lblTrialNumber.Text = "Trial Number";
            // 
            // txtSubjectName
            // 
            this.txtSubjectName.Location = new System.Drawing.Point(6, 36);
            this.txtSubjectName.Name = "txtSubjectName";
            this.txtSubjectName.Size = new System.Drawing.Size(181, 20);
            this.txtSubjectName.TabIndex = 1;
            // 
            // lblSubjectName
            // 
            this.lblSubjectName.AutoSize = true;
            this.lblSubjectName.Location = new System.Drawing.Point(7, 20);
            this.lblSubjectName.Name = "lblSubjectName";
            this.lblSubjectName.Size = new System.Drawing.Size(74, 13);
            this.lblSubjectName.TabIndex = 0;
            this.lblSubjectName.Text = "Subject Name";
            // 
            // chkInvisibleMirror
            // 
            this.chkInvisibleMirror.AutoSize = true;
            this.chkInvisibleMirror.Location = new System.Drawing.Point(12, 285);
            this.chkInvisibleMirror.Name = "chkInvisibleMirror";
            this.chkInvisibleMirror.Size = new System.Drawing.Size(213, 17);
            this.chkInvisibleMirror.TabIndex = 6;
            this.chkInvisibleMirror.Text = "Use Invisible Hands During Mirror Mode";
            this.chkInvisibleMirror.UseVisualStyleBackColor = true;
            this.chkInvisibleMirror.CheckedChanged += new System.EventHandler(this.chkInvisibleMirror_CheckedChanged);
            // 
            // grpHandOptions
            // 
            this.grpHandOptions.Controls.Add(this.radioButton3);
            this.grpHandOptions.Controls.Add(this.radioButton2);
            this.grpHandOptions.Controls.Add(this.radioButton1);
            this.grpHandOptions.Location = new System.Drawing.Point(212, 163);
            this.grpHandOptions.Name = "grpHandOptions";
            this.grpHandOptions.Size = new System.Drawing.Size(103, 98);
            this.grpHandOptions.TabIndex = 7;
            this.grpHandOptions.TabStop = false;
            this.grpHandOptions.Text = "Hand Options";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 68);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(88, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Disable Right";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 45);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(81, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Disable Left";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 21);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(83, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Enable Both";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // grpDAQoptions
            // 
            this.grpDAQoptions.Controls.Add(this.nudPortNumber);
            this.grpDAQoptions.Controls.Add(this.lblPortNumber);
            this.grpDAQoptions.Controls.Add(this.nudDeviceNumber);
            this.grpDAQoptions.Controls.Add(this.lblDeviceNumber);
            this.grpDAQoptions.Location = new System.Drawing.Point(130, 62);
            this.grpDAQoptions.Name = "grpDAQoptions";
            this.grpDAQoptions.Size = new System.Drawing.Size(184, 82);
            this.grpDAQoptions.TabIndex = 8;
            this.grpDAQoptions.TabStop = false;
            // 
            // lblDeviceNumber
            // 
            this.lblDeviceNumber.AutoSize = true;
            this.lblDeviceNumber.Location = new System.Drawing.Point(7, 19);
            this.lblDeviceNumber.Name = "lblDeviceNumber";
            this.lblDeviceNumber.Size = new System.Drawing.Size(81, 13);
            this.lblDeviceNumber.TabIndex = 0;
            this.lblDeviceNumber.Text = "Device Number";
            // 
            // nudDeviceNumber
            // 
            this.nudDeviceNumber.Location = new System.Drawing.Point(94, 16);
            this.nudDeviceNumber.Name = "nudDeviceNumber";
            this.nudDeviceNumber.Size = new System.Drawing.Size(41, 20);
            this.nudDeviceNumber.TabIndex = 1;
            // 
            // lblPortNumber
            // 
            this.lblPortNumber.AutoSize = true;
            this.lblPortNumber.Location = new System.Drawing.Point(7, 51);
            this.lblPortNumber.Name = "lblPortNumber";
            this.lblPortNumber.Size = new System.Drawing.Size(66, 13);
            this.lblPortNumber.TabIndex = 2;
            this.lblPortNumber.Text = "Port Number";
            // 
            // nudPortNumber
            // 
            this.nudPortNumber.Location = new System.Drawing.Point(94, 49);
            this.nudPortNumber.Name = "nudPortNumber";
            this.nudPortNumber.Size = new System.Drawing.Size(41, 20);
            this.nudPortNumber.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 341);
            this.Controls.Add(this.grpDAQoptions);
            this.Controls.Add(this.grpHandOptions);
            this.Controls.Add(this.chkInvisibleMirror);
            this.Controls.Add(this.grpLogging);
            this.Controls.Add(this.chkUseDAQ);
            this.Controls.Add(this.grpVR);
            this.Controls.Add(this.btnSelectConfig);
            this.Controls.Add(this.txtConfiguration);
            this.Controls.Add(this.btnLaunch);
            this.Name = "Form1";
            this.Text = "Move Watch Launcher";
            this.grpVR.ResumeLayout(false);
            this.grpVR.PerformLayout();
            this.grpLogging.ResumeLayout(false);
            this.grpLogging.PerformLayout();
            this.grpHandOptions.ResumeLayout(false);
            this.grpHandOptions.PerformLayout();
            this.grpDAQoptions.ResumeLayout(false);
            this.grpDAQoptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDeviceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPortNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLaunch;
        private System.Windows.Forms.TextBox txtConfiguration;
        private System.Windows.Forms.Button btnSelectConfig;
        private System.Windows.Forms.GroupBox grpVR;
        private System.Windows.Forms.RadioButton rdoNonVR;
        private System.Windows.Forms.RadioButton rdoVR;
        private System.Windows.Forms.CheckBox chkUseDAQ;
        private System.Windows.Forms.GroupBox grpLogging;
        private System.Windows.Forms.TextBox txtTrialNumber;
        private System.Windows.Forms.Label lblTrialNumber;
        private System.Windows.Forms.TextBox txtSubjectName;
        private System.Windows.Forms.Label lblSubjectName;
        private System.Windows.Forms.CheckBox chkInvisibleMirror;
        private System.Windows.Forms.GroupBox grpHandOptions;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton rdoCyberGlove;
        private System.Windows.Forms.RadioButton rdo5DT;
        private System.Windows.Forms.GroupBox grpDAQoptions;
        private System.Windows.Forms.Label lblDeviceNumber;
        private System.Windows.Forms.NumericUpDown nudPortNumber;
        private System.Windows.Forms.Label lblPortNumber;
        private System.Windows.Forms.NumericUpDown nudDeviceNumber;
    }
}

